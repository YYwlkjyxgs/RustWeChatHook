# RustWeChatHook

#### 介绍
Rust编写的WeChat远程Hook，本项目的代码并不是很好，可以作为一个参考，但请不要将它视为主要部分，目前只实现一些比较简单的部分（微信版本、登录状态、登录二维码、个人信息等等），本项目还支持多开微信（自行研究，已经有示例了），WeChat只支持3.6.0.18版本

#### 使用说明
本项目只支持windows平台且32位运行
