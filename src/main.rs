extern crate alloc;

mod win;
mod hook;

use hook::wechat;

fn main() {
    wechat::disable_dep(); // 禁用自身DEP执行保护
    let we_chat_list = wechat::get_processes_wechat(-1);
    println!("{:?}", we_chat_list); // 获取 WeChat PID
    for i in we_chat_list.iter() {
        unsafe {
            println!("{} Version: {}", i, wechat::get_wechat_version(*i));
            println!("{} Login QRCode: {}", i, wechat::get_wechat_login_qrcode(*i));
            println!("{} Login State: {}", i, wechat::get_wechat_login_state(*i));
            wechat::get_wechat_personal_data(*i);
            println!("-----------------------------------------------------------------------");
        }
    }
}