extern crate libloading;

use libloading::{Library, Symbol};

pub fn get_system_deppolicy() -> u32 {
    type SysGetLibVersion = fn() -> u32;
    let lib = Library::new(r"C:\Windows\System32\kernel32.dll").unwrap();
    unsafe {
        let function: Symbol<SysGetLibVersion> = lib.get(b"GetSystemDEPPolicy").unwrap();
        return function();
    }
}

pub fn set_process_deppolicy(dw_flags: u32) -> bool {
    type SysGetLibVersion = fn(dwFlags: u32) -> bool;
    let lib = Library::new(r"C:\Windows\System32\kernel32.dll").unwrap();
    unsafe {
        let function: Symbol<SysGetLibVersion> = lib.get(b"SetProcessDEPPolicy").unwrap();
        return function(dw_flags);
    }
}